package com.example.steve.bums;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class CameraActivity extends ActionBarActivity {

    public static final String TAG = "CameraFragment";

    private Camera camera;
    ProgressBar progressBar;
    private SurfaceView surfaceView;
    private ImageButton photoButton;
    String encodedImage = "";
    GlobalVars state;


    String user_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);


        photoButton = (ImageButton) findViewById(R.id.camera_photo_button);
        state = ((GlobalVars) getApplicationContext());
        user_id = state.getSomeVariable();
        progressBar = (ProgressBar) findViewById(R.id.progressBar4);
        progressBar.setVisibility(View.INVISIBLE);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        if (camera == null) {
            try {
                camera = Camera.open();
                photoButton.setEnabled(true);
            } catch (Exception e) {
                Log.e(TAG, "No camera with exception: " + e.getMessage());
                photoButton.setEnabled(false);
                Toast.makeText(this, "No camera detected",
                        Toast.LENGTH_LONG).show();
            }
        }

        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (camera == null)
                    return;
                camera.takePicture(new Camera.ShutterCallback() {

                    @Override
                    public void onShutter() {
                        // nothing to do
                    }

                }, null, new Camera.PictureCallback() {

                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {
                        saveScaledPhoto(data);
                    }

                });

            }
        });

        surfaceView = (SurfaceView) findViewById(R.id.camera_surface_view);
        SurfaceHolder holder = surfaceView.getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {

            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (camera != null) {
                        camera.setDisplayOrientation(90);
                        camera.setPreviewDisplay(holder);
                        camera.startPreview();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Error setting up preview", e);
                }
            }

            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
                // nothing to do here
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                //camera.get
                //surfaceView.getHolder().removeCallback((SurfaceHolder.Callback) surfaceView);
                //camera.stopPreview();
                camera.release();
            }

        });
    }
    private void saveScaledPhoto(byte[] data) {

        // Resize photo from camera byte array
        Bitmap photoImage = BitmapFactory.decodeByteArray(data, 0, data.length);
        Bitmap photoImageScaled = Bitmap.createScaledBitmap(photoImage, 600, 600
                * photoImage.getHeight() / photoImage.getWidth(), false);

        // Override Android default landscape orientation and save portrait
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap rotatedScaledPhotoImage = Bitmap.createBitmap(photoImageScaled, 0,
                0, photoImageScaled.getWidth(), photoImageScaled.getHeight(),
                matrix, true);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        //Bitmap rotatedScaledPhotoImage;
        rotatedScaledPhotoImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);

        byte[] scaledData = bos.toByteArray();

        encodedImage = Base64.encodeToString(scaledData, Base64.DEFAULT);

        // System.out.println("EncodedImage:");
        //System.out.print(encodedImage);
        //System.out.println();
        //System.out.println("The End");


        new CameraPost().execute();


    }



    class CameraPost extends AsyncTask<Void, Void, Integer> {


        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;
            String result = "";
            int status = -1;
            // Save the scaled image to Parse


            try {
                String json = "";

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/pictures");

                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("owner_id", user_id);
                jsonObject.accumulate("encoded_pic", encodedImage);
                jsonObject.accumulate("album_id", CameraActivity.this.getAlbumId());
                Log.d("damn", (CameraActivity.this.getAlbumId()));
                Log.d("damn", user_id);
                json = jsonObject.toString(); // Output to string

                StringEntity se = new StringEntity(json);
                // put json string into server
                httppost.setEntity(se);
                httppost.setHeader("Accept", "application/json");
                httppost.setHeader("Content-type", "application/json");
                HttpResponse httpResponse = httpclient.execute(httppost);

                //debug
                Log.d("className", json);

                //receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();

                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

                Log.d("result", result);
                Log.d("status",Integer.toString(status));

            }catch(Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            return status;
        }

        protected void onPostExecute (Integer status) {

            if(status == 201){
                Toast.makeText(CameraActivity.this.getApplicationContext(), "Image Uploaded",
                        Toast.LENGTH_LONG).show();
                Intent open = new Intent(CameraActivity.this, ViewAlbumActivity.class);
                open.putExtra("album_Id", getAlbumId());
                open.putExtra("album_name", getAlbumName());
                startActivity(open);
                finish();

            }else{
                //not created.
                Toast.makeText(CameraActivity.this.getApplicationContext(), "Failed to Upload Image",
                        Toast.LENGTH_LONG).show();
                Intent open = new Intent(CameraActivity.this, ViewAlbumActivity.class);
                open.putExtra("album_Id", getAlbumId());
                open.putExtra("album_name", getAlbumName());
                startActivity(open);
                finish();
            }
            progressBar.setVisibility(View.INVISIBLE);

        }


    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
    /*
     * Once the photo has saved successfully, we're ready to return to the
     * NewMealFragment. When we added the CameraFragment to the back stack, we
     * named it "NewMealFragment". Now we'll pop fragments off the back stack
     * until we reach that Fragment.
     */
    /*private void addPhotoToMealAndReturn(ParseFile photoFile) {
        ((NewMealActivity) getActivity()).getCurrentMeal().setPhotoFile(
                photoFile);
        FragmentManager fm = getActivity().getFragmentManager();
        fm.popBackStack("NewMealFragment",
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }*/



    @Override
    public void onResume() {
        super.onResume();
        if (camera == null) {
            try {
                camera = Camera.open();
                photoButton.setEnabled(true);
            } catch (Exception e) {
                Log.i(TAG, "No camera: " + e.getMessage());
                photoButton.setEnabled(false);
                Toast.makeText(this, "No camera detected",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onPause() {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
        }
        super.onPause();
    }

    public String getAlbumId(){
        Bundle extras;
        extras = getIntent().getExtras();
        return extras.getString("album_Id");
    }

    public String getAlbumName(){
        Bundle extras;
        extras = getIntent().getExtras();
        return extras.getString("album_name");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_album, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
