package com.example.steve.bums;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class GotoFriendProfile extends ActionBarActivity {


    ImageView userPic;
    ProgressBar progressBar7;

    String result = "";
    String result2 = "";
    String result3 = "";
    List<String> albums;
    private List<String> albumPic;
    private List<Bitmap> bitPic;
    private List<String> test;
    TextView userNameDisplay;
    private List<String> albumId;
    TextView friendCounts;
    Button addFriendBut;
    Bitmap temp;
    Bitmap scaledBitmap;

    private Fragment tabsFragment;
    GlobalVars state;
    private FragmentManager manager;
    String id_str ="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goto_friend_profile);

        state = ((GlobalVars) getApplicationContext());
        id_str = state.getSomeVariable();

        albumId = new ArrayList<String>();
        albumPic = new ArrayList<String>();
        bitPic = new ArrayList<Bitmap>();
        progressBar7 = (ProgressBar) findViewById(R.id.progressBar7);
        progressBar7.setVisibility(View.INVISIBLE);
        userNameDisplay = (TextView) findViewById(R.id.original_user_name);
        userNameDisplay.setText(state.getfriendName());
        friendCounts = (TextView) findViewById(R.id.Description);
        userPic = (ImageView) findViewById(R.id.original_profile);
        addFriendBut = (Button) findViewById(R.id.btn_addFriends1);


        manager = getFragmentManager();
        tabsFragment = manager.findFragmentById(R.id.fragmentContainer2);

        if(tabsFragment == null) {
            tabsFragment = new TabsFragment();
            manager.beginTransaction().add(R.id.fragmentContainer2, tabsFragment)
                    .commit();
        }

/*
        Button addNew = (Button)findViewById(R.id.btn_addNew);
        addNew.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startAddNewAlbum();
                ProfileActivity.this.destroyFragment();

            }
        });

        Button addFriends = (Button)findViewById(R.id.btn_addFriends);
        addNew.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startAddFriends();
            }
        });
*/
        if(state.getfriendName().equals("kobe"))
            userPic.setImageResource(R.drawable.kobe);
        else if(state.getfriendName().equals("jeremylin"))
            userPic.setImageResource(R.drawable.lin);
        else
            userPic.setImageResource(R.drawable.lakers);

        //Create a set of dummy strings to be displayed
        albums = new ArrayList<String>();
        //albums.add("as");
        //hide Action Bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        new PopluateGrid().execute();


    }

    public void destroyFragment(){

        if(tabsFragment != null)
            manager.beginTransaction().remove(tabsFragment).commit();
        tabsFragment = null;
    }

    class PopluateGrid extends AsyncTask<Void, Void, Integer> {


        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar7.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }
        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;
            InputStream inputStream3 = null;

            int status = -1;

            try{

                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/users/"+ state.getfriendId() + "/albums"));

                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();


                // convert inputstream to string
                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            }catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            Log.d("result", result);
            Log.d("status",Integer.toString(status));

            try {
                JSONArray array = new JSONArray(result);
                for (int i = 0; i < array.length(); i++) {
                    InputStream inputStream2 = null;
                    JSONObject currObject = array.getJSONObject(i);
                    albums.add( currObject.getString("name") );
                    albumId.add (currObject.getString("id"));
                    /*
                    //test
                    JSONArray jsonMainArr = currObject.getJSONArray("contributor_ids");
                    String temp[] = new String[jsonMainArr.length()];
                        for(int j = 0; j < temp.length; j++){
                            Log.d("name", temp[j]);

                        }
                    //end test
*/
                    Log.d("name", albums.get(i));
                    Log.d("id", albumId.get(i));

                    try {
                        HttpClient httpclient2 = new DefaultHttpClient();
                        HttpResponse httpResponse2 = httpclient2.execute(new HttpGet("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/albums/" + albumId.get(i) + "/pictures"));


                        inputStream2 = httpResponse2.getEntity().getContent();
                        //receive response as code
                        status = httpResponse2.getStatusLine().getStatusCode();


                        // convert inputstream to string
                        if(inputStream2 != null)
                            result2 = convertInputStreamToString(inputStream2);
                        else
                            result2 = "Did not work!";

                        Log.d("result2", result2);



                    }catch (Exception e) {
                        Log.d("InputStream2", e.getLocalizedMessage());
                    }


                    try {
                        JSONArray array2 = new JSONArray(result2);
                        if(array2.length() > 0) {
                            JSONObject currObject2 = array2.getJSONObject(0);
                            //Log.d("in obj", currObject2.getString("encoded_pic"));
                            albumPic.add (currObject2.getString("encoded_pic"));
                            //Log.d("name", albumPic.get(i));
                            try {
                                byte[] decodedString = Base64.decode(albumPic.get(i), Base64.DEFAULT);
                                //InputStream in = new java.net.URL(albumPic.get(i)).openStream();
                                temp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                scaledBitmap = Bitmap.createScaledBitmap(temp, 100, 50,true);
                                bitPic.add(scaledBitmap);
                                // in.reset();
                            } catch (Exception e) {
                                Log.e("Error", e.getMessage());
                                e.printStackTrace();
                            }
                        }//if
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                HttpClient httpclient3 = new DefaultHttpClient();
                HttpResponse httpResponse3 = httpclient3.execute(new HttpGet("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/users/" + state.getSomeVariable()));


                inputStream3 = httpResponse3.getEntity().getContent();
                //receive response as code
                status = httpResponse3.getStatusLine().getStatusCode();


                // convert inputstream to string
                if(inputStream3 != null)
                    result3 = convertInputStreamToString(inputStream3);
                else
                    result3 = "Did not work!";

                Log.d("result2", result3);

                try{
                    JSONObject currObject3 = new JSONObject(result3);
                    state.setFriendCount(currObject3.getString("friends_count"));
                }catch (JSONException e) {
                    e.printStackTrace();
                }

            }catch (Exception e) {
                Log.d("InputStream2", e.getLocalizedMessage());
            }



            return status;

        }
        protected void onPostExecute (Integer status){

            progressBar7.setVisibility(View.INVISIBLE);



            // Create the adapter passing a reference to the XML layout for each row
            // and a reference to the EditText (or TextView) in the item XML layout
            ArrayAdapter adapter = new ArrayAdapter(GotoFriendProfile.this,
                    R.layout.home_grid_items, albums) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View row = convertView;
                    final int pos = position;
                    if (row == null) {
                        //getting custom layout for the row
                        LayoutInflater inflater = GotoFriendProfile.this.getLayoutInflater();//LayoutInflater.from(getActivity());
                        row = inflater.inflate(R.layout.home_grid_items, parent, false);
                    }

                    //get the reference to the EditText of your row.
                    // find the item with row.findViewById()
                    // EditText nameField = (EditText)row.findViewById(R.id.name);
                    ImageView albumPic = (ImageView) row.findViewById(R.id.album_pic);

                    TextView albumNameField = (TextView) row.findViewById(R.id.pic_name);
                    Log.d("position out", Integer.toString(position));
                    albumNameField.setText(albums.get(position));

                    if (bitPic.size() > position) {
                        Log.d("position", Integer.toString(position));
                        albumPic.setImageBitmap(bitPic.get(position));
                    }


                    return row; //the row that ListView draws
                }
            };

            friendCounts.setText(state.getFriendCount());
            // Get a reference to our ListView
            if(Integer.parseInt(state.getFriendCount()) > 1){
                addFriendBut.setText("UnFriend");
            }else
                addFriendBut.setText("Add Friend");

            GridView gridView = (GridView) findViewById(R.id.gridView);

            // Set the adapter on the List View
            gridView.setAdapter(adapter);

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // Toast.makeText(getApplicationContext(),
                    //"Click ListItem Number " + position, Toast.LENGTH_LONG)
                    //.show();
                    Intent open = new Intent(GotoFriendProfile.this, ViewAlbumActivity.class);
                    state.setAblumID(albumId.get(position));
                    state.setAblumName(albums.get(position));
                    open.putExtra("album_Id", state.getAblumID());
                    open.putExtra("album_name", state.getAblumName());
                    //use album name to interact with data base
                    //EditText editText = (EditText) findViewById(R.id.edit_message);
                    //String message = editText.getText().toString();
                    //intent.putExtra(EXTRA_MESSAGE, message);
                    startActivity(open);
                    finish();
                }
            });


        }


    }



    class PutImageInGrid extends AsyncTask<Void, Void, Integer> {

        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar7.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return null;
        }

        protected void onPostExecute (Integer status) {

            progressBar7.setVisibility(View.INVISIBLE);
        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result2 = "";
        while((line = bufferedReader.readLine()) != null)
            result2 += line;

        inputStream.close();
        return result2;

    }



    public void startAddFriends(View v){
        new AddFriendTask().execute();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getUserId(){

        return id_str;
    }

    public String getUserName(){

        return state.getUserName();
    }

    class AddFriendTask extends AsyncTask<Void, Void, Integer> {

        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar7.setVisibility(View.INVISIBLE);

        }

        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;

            int status2 = -1;

            try{
                String json = "";
                // create HttpClient
                HttpClient httpclient2 = new DefaultHttpClient();

                HttpPut httpPut = new HttpPut("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/users/" + state.getSomeVariable()+ "/add_friend");

                // receive response as inputStream


                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("username", state.getfriendName());
                json = jsonObject.toString();
                StringEntity se = new StringEntity(json);
                httpPut.setEntity(se);
                httpPut.setHeader("Accept", "application/json");
                httpPut.setHeader("Content-type", "application/json");
                //debug
                Log.d("user to Add", json);

                HttpResponse httpResponse2 = httpclient2.execute(httpPut);
                inputStream = httpResponse2.getEntity().getContent();
                //receive response as code
                status2 = httpResponse2.getStatusLine().getStatusCode();

                // convert inputstream to string
                if(inputStream != null)
                    result2 = convertInputStreamToString(inputStream);
                else
                    result2 = "Did not work!";

                Log.d("result2", result2);
                Log.d("status2",Integer.toString(status2));

            }catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            return status2;
        }

        protected void onPostExecute(Integer status) {
            //textView.setText("Hello !!!");
            progressBar7.setVisibility(View.INVISIBLE);
            if(status == 200){
                Toast.makeText(getApplicationContext(), "Friend Added",
                        Toast.LENGTH_LONG).show();
                friendCounts.setText(state.getFriendCount() + 1);

            }else{

                Toast.makeText(getApplicationContext(), "Failed to add friend",
                        Toast.LENGTH_LONG).show();
            }

        }
    }
}

