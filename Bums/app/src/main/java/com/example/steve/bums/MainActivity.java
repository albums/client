package com.example.steve.bums;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MainActivity extends ActionBarActivity {

    ProgressBar progressBar;
    EditText emailAddress;
    EditText password;
    Intent intent;

    String id_str = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emailAddress = (EditText)findViewById(R.id.email_address);
        password = (EditText)findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);


        //hide Action Bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();


    }


    public void reg(View v) {

        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        //Toast.makeText(getApplicationContext(), "this is my Toast message!!! =)",
                //Toast.LENGTH_LONG).show();
    }


    public void signIn(View v) {


        intent = new Intent(this, ViewActivity.class);
        if(emailAddress.getText().length() == 0 || password.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "One or more fields is/are blank",
            Toast.LENGTH_LONG).show();
        }else{
            new LoginTask().execute();
        }


        //Toast.makeText(getApplicationContext(), "this is my Toast message!!! =)",
        //Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class LoginTask extends AsyncTask <Void, Void, Integer> {

        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;
            String result = "";
            int status = -1;

            try{
                String json = "";
                //connect to server
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/users/login");

                //parsing JSON
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("username", emailAddress.getText().toString());
                jsonObject.accumulate("password", password.getText().toString());
                json = jsonObject.toString();

                StringEntity se = new StringEntity(json);
                // put json string into server
                httppost.setEntity(se);
                httppost.setHeader("Accept", "application/json");
                httppost.setHeader("Content-type", "application/json");
                HttpResponse httpResponse = httpclient.execute(httppost);


                //debug
                Log.d("className", json);

                //receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();

                if(inputStream != null) {
                    result = convertInputStreamToString(inputStream);

                    //get user ID
                    JSONObject jObject = new JSONObject(result);
                    String aJsonString = jObject.getString("id");
                    id_str = aJsonString;
                    Log.d("id", aJsonString);
                }
                else
                    result = "Did not work!";

                Log.d("result", result);
                Log.d("status",Integer.toString(status));



            }catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }
            //progressBar.setVisibility(View.INVISIBLE);
            return status;
        }

        protected void onPostExecute (Integer status){
            if(status == 200){

                //setting user ID in as a global variable
                GlobalVars state = ((GlobalVars) getApplicationContext());
                state.setSomeVariable(id_str);
                state.setUserName(emailAddress.getText().toString());

                Toast.makeText(getApplicationContext(), "Login successfully",
                Toast.LENGTH_LONG).show();
                startActivity(intent);

            }else{
                Toast.makeText(getApplicationContext(), "Unable to login",
                Toast.LENGTH_LONG).show();

            }
            progressBar.setVisibility(View.INVISIBLE);


        }


    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}


