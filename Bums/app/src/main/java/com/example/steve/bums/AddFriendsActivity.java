package com.example.steve.bums;

import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;



public class AddFriendsActivity extends ActionBarActivity {
    private List<String> users;
    GlobalVars state;
    String user_name ="";
    private List<String>  userName;
    private List<String>  userFriendSearchID;
    String result = "";
    String result2 = "";
    ProgressBar progressBar4;
    private EditText inputSearch;
    ArrayAdapter adapter;
    String userNameToAdd;
    JSONArray array;
    JSONObject currObject;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friends);
        userName = new ArrayList();
        userFriendSearchID = new ArrayList();
        state = ((GlobalVars) getApplicationContext());
        user_name = state.getUserName();
        inputSearch = (EditText) findViewById(R.id.searchBar);


        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        progressBar4 = (ProgressBar) findViewById(R.id.progressBar8);
        progressBar4.setVisibility(View.INVISIBLE);

        new PopluateList().execute();

    }


    class PopluateList extends AsyncTask<Void, Void, Integer> {


        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar4.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }
        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;

            int status = -1;

            try{

                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/users"));

                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();

                // convert inputstream to string
                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            }catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }


            try {
                array = new JSONArray(result);
                for (int i = 0; i < array.length(); i++) {
                    currObject = array.getJSONObject(i);
                    if(!currObject.getString("username").equals(user_name)) {
                        userName.add(currObject.getString("username"));
                        userFriendSearchID.add(currObject.getString("id"));
                    }
                    //Log.d("name", userName.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }





            return status;

        }
        protected void onPostExecute (Integer status){

            progressBar4.setVisibility(View.INVISIBLE);


                adapter = new ArrayAdapter<String>(AddFriendsActivity.this,
                    R.layout.friends_list_item, userName);



            // Get a reference to our ListView
            ListView listView1 = (ListView) findViewById(R.id.listView2);

            // Set the adapter on the List View
            listView1.setAdapter(adapter);
            listView1.setTextFilterEnabled(true);

            listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // When clicked, show a toast with the TextView text
                    //Toast.makeText(getApplicationContext(),
                            //((TextView) view).getText(), Toast.LENGTH_SHORT).show();
                    state.setfriendName(((TextView) view).getText().toString());
                    state.setfriendId(userFriendSearchID.get(position));
                    Intent intent = new Intent(view.getContext(), GotoFriendProfile.class);
                    startActivity(intent);
                }
            });

            inputSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    // When user changed the Text
                    adapter.getFilter().filter(cs.toString());
                    //adapter.getFilter().filter(cs);
                    //adapter.notifyDataSetChanged();
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    //String text = inputSearch.getText().toString().toLowerCase(Locale.getDefault());
                    //adapter.getFilter().filter(text);
                }
            });

        }


    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result2 = "";
        while((line = bufferedReader.readLine()) != null)
            result2 += line;

        inputStream.close();
        return result2;

    }

    class AddFriendTask extends AsyncTask<Void, Void, Integer> {

        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar4.setVisibility(View.INVISIBLE);

        }

        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;

            int status2 = -1;

            try{
                String json = "";
                // create HttpClient
                HttpClient httpclient2 = new DefaultHttpClient();

                HttpPut httpPut = new HttpPut("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/users/" + state.getSomeVariable()+ "/add_friend");

                // receive response as inputStream


                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("username", userNameToAdd);
                json = jsonObject.toString();
                StringEntity se = new StringEntity(json);
                httpPut.setEntity(se);
                httpPut.setHeader("Accept", "application/json");
                httpPut.setHeader("Content-type", "application/json");
                //debug
                Log.d("user to Add", json);

                HttpResponse httpResponse2 = httpclient2.execute(httpPut);
                inputStream = httpResponse2.getEntity().getContent();
                //receive response as code
                status2 = httpResponse2.getStatusLine().getStatusCode();

                // convert inputstream to string
                if(inputStream != null)
                    result2 = convertInputStreamToString(inputStream);
                else
                    result2 = "Did not work!";

                Log.d("result2", result2);
                Log.d("status2",Integer.toString(status2));

            }catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            return status2;
        }

        protected void onPostExecute(Integer status) {
            //textView.setText("Hello !!!");
            progressBar4.setVisibility(View.INVISIBLE);
            if(status == 200){
                Toast.makeText(getApplicationContext(), "Friend Added",
                        Toast.LENGTH_LONG).show();

                Intent open = new Intent(AddFriendsActivity.this, ProfileActivity.class);

                //use album name to interact with data base
                //EditText editText = (EditText) findViewById(R.id.edit_message);
                //String message = editText.getText().toString();
                open.putExtra(ViewAlbumActivity.EXTRA_MESSAGE, "PROFILE");
                startActivity(open);
                finish();
            }else{

                Toast.makeText(getApplicationContext(), "Failed to add friend",
                        Toast.LENGTH_LONG).show();
                Intent open = new Intent(AddFriendsActivity.this, ProfileActivity.class);

                //use album name to interact with data base
                //EditText editText = (EditText) findViewById(R.id.edit_message);
                //String message = editText.getText().toString();
                open.putExtra(ViewAlbumActivity.EXTRA_MESSAGE, "PROFILE");
                startActivity(open);
                finish();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public String getUserName(){

        return state.getUserName();
    }

/*
    public class HttpPatch extends HttpEntityEnclosingRequestBase {

        public final static String METHOD_NAME = "PATCH";

        public HttpPatch() {
            super();
        }

        public HttpPatch(final URI uri) {
            super();
            setURI(uri);
        }

        public HttpPatch(final String uri) {
            super();
            setURI(URI.create(uri));
        }

        @Override
        public String getMethod() {
            return METHOD_NAME;
        }

    }
*/
}
