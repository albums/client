package com.example.steve.bums;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class AddNewAlbumActivity extends ActionBarActivity {

    private Fragment addFriendsFragment;
    private FragmentManager manager;
    //String contributers [] = new String[99];
    GlobalVars state;
    EditText albumName;
    EditText location;
    TextView inviFriends;
    EditText albumDescrip;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_album);

        albumName = (EditText)findViewById(R.id.field_album_name);
        location = (EditText)findViewById(R.id.field_loc);
        inviFriends = (TextView)findViewById(R.id.field_friends);
        albumDescrip = (EditText)findViewById(R.id.field_des);

        progressBar = (ProgressBar) findViewById(R.id.progressBar6);
        progressBar.setVisibility(View.INVISIBLE);

        ActionBar actionBar = getSupportActionBar();
        //actionBar.setTitle("Create New Album");
        actionBar.hide();
        state = ((GlobalVars) getApplicationContext());

        ImageButton addFriends = (ImageButton)findViewById(R.id.btn_add_friends);
        addFriends.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                manager = getFragmentManager();

                if(addFriendsFragment == null) {
                    addFriendsFragment = new AddFriendsFragment();
                    manager.beginTransaction().add(R.id.addFriendsFragment, addFriendsFragment)
                            .commit();
                }
            }
        });



    }

    public String getUserName(){

        return state.getUserName();
    }

    public void setConUser(String user){

        String[] c = new String[99];
        state.setConU(user);

        c = state.getConU();

        for(int i = 0; i < state.getCount(); i++){

            Log.d("User added", c[i]);
        }

    }
    public void addAlbum(View v) {

        new addAlbum2().execute();
        //Toast.makeText(getApplicationContext(), "this is my Toast message!!! =)",
        //Toast.LENGTH_LONG).show();
    }

    class addAlbum2 extends AsyncTask<Void, Void, Integer> {

        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;
            String result = "";
            int status = -1;

            try{
                String json = "";
                //connect to server
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/albums");

                //parsing JSON
                JSONObject jsonObject = new JSONObject();
                JSONArray jArray = new JSONArray();
                jsonObject.accumulate("name", albumName.getText().toString());
                jsonObject.accumulate("description", albumDescrip.getText().toString());
                jsonObject.accumulate("owner_id", state.getSomeVariable());
                //jArray.put(getUserName());
                if(state.getCount() != 0){
                    String[] c;
                    c = state.getConU();
                    for(int i = 0; i < state.getCount(); i++) {
                        jArray.put(c[i]);
                        //jsonObject.accumulate("contributor_ids", c[i]);

                    }
                    jsonObject.accumulate("user_ids", jArray);
                }

                json = jsonObject.toString();

                StringEntity se = new StringEntity(json);
                // put json string into server
                httppost.setEntity(se);
                httppost.setHeader("Accept", "application/json");
                httppost.setHeader("Content-type", "application/json");
                HttpResponse httpResponse = httpclient.execute(httppost);


                //debug
                Log.d("className", json);

                //receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();

                if(inputStream != null) {
                    result = convertInputStreamToString(inputStream);

                    //get user ID
                    //JSONObject jObject = new JSONObject(result);
                    //String aJsonString = jObject.getString("id");
                    //id_str = aJsonString;
                    //Log.d("id", aJsonString);
                }
                else
                    result = "Did not work!";

                Log.d("result", result);
                Log.d("status",Integer.toString(status));



            }catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }
            //progressBar.setVisibility(View.INVISIBLE);
            return status;
        }

        protected void onPostExecute (Integer status){

            progressBar.setVisibility(View.INVISIBLE);

            if(status == 201){

                Toast.makeText(getApplicationContext(), "Album Created",
                Toast.LENGTH_LONG).show();


                Intent open = new Intent(AddNewAlbumActivity.this, ProfileActivity.class);

                //use album name to interact with data base
                //EditText editText = (EditText) findViewById(R.id.edit_message);
                //String message = editText.getText().toString();
                open.putExtra(ViewAlbumActivity.EXTRA_MESSAGE, "PROFILE");
                startActivity(open);
            }else{

                Toast.makeText(getApplicationContext(), "Album Not Create",
                Toast.LENGTH_LONG).show();
            }

            state.resetCounter();
            state.resetConU();


        }


    }

    public void destroyFragment(){
        if(addFriendsFragment != null)
            manager.beginTransaction().remove(addFriendsFragment).commit();
            addFriendsFragment = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_new_album, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
