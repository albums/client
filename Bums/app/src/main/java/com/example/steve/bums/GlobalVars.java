package com.example.steve.bums;

import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Steve on 2/21/2015.
 */
public class GlobalVars extends Application {

    private String someVariable="";//userID

    private String userName2="";
    private String fCount = "";
    private String fName = "";
    private String fID = "";
    private String albumID = "";
    private String albumName = "";
    private String contributeUser[] = new String[99];
    int counter = 0;
    //String encImage;

    //user ID
    public String getSomeVariable() {
        return someVariable;
    }

    public void setSomeVariable(String someVariable) {
        this.someVariable = someVariable;
    }

    //get friend count
    public void setFriendCount(String fCount) {
        this.fCount = fCount;
    }

    public String getFriendCount() {
        return fCount;
    }

    //to friend profile
    //get friend ID
    public String getfriendId() {
        return fID;
    }

    public void setfriendId(String fID) {this.fID = fID;}

    //get Friend username
    public String getfriendName() {
        return fName;
    }

    public void setfriendName(String fName) {this.fName = fName;}

    //contributor
    public String[] getConU() {
        return contributeUser;
    }

    public void setConU(String contributeUser) {
        this.contributeUser[counter] = contributeUser;
        counter ++;
    }

    public void resetConU() {

        for(int i = 0; i < 99; i++)
            this.contributeUser[i] = null;

    }

    public void resetCounter() {
        counter = 0;
    }

    public int getCount() {
        return counter;
    }

    //userName
    public String getUserName() {
        return userName2;
    }

    public void setUserName(String userName2) {
        this.userName2 = userName2;
    }

    //gobal album ID
    public String getAblumID() {
        return albumID;
    }

    public void setAblumID(String albumID) {
        this.albumID = albumID;
    }

    public String getAblumName() {
        return albumName;
    }

    public void setAblumName(String albumName) {
        this.albumName = albumName;
    }

/*
    public void hardUpload(){

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.e);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); //compress to which format you want.
        byte [] byte_arr = stream.toByteArray();
        encImage = Base64.encodeToString(byte_arr, Base64.DEFAULT);
        new CameraPost().execute();



    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    class CameraPost extends AsyncTask<Void, Void, Integer> {


        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;
            String result = "";
            int status = -1;
            // Save the scaled image to Parse


            try {
                String json = "";

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/pictures");

                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("owner_id", "63dfe123-ff33-43f5-aef9-cc080c6d90bf");
                jsonObject.accumulate("encoded_pic", encImage);
                jsonObject.accumulate("album_id", "f0f92ac7-52d4-41e8-ae00-e3d3aa1bd3c3");

                json = jsonObject.toString(); // Output to string

                StringEntity se = new StringEntity(json);
                // put json string into server
                httppost.setEntity(se);
                httppost.setHeader("Accept", "application/json");
                httppost.setHeader("Content-type", "application/json");
                HttpResponse httpResponse = httpclient.execute(httppost);

                //debug
                Log.d("className", json);

                //receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();

                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

                Log.d("result", result);
                Log.d("status",Integer.toString(status));

            }catch(Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            return status;
        }

        protected void onPostExecute (Integer status) {

            if(status == 201){
                Log.d("201", "Uploaded");


            }else{
                //not created.
                Log.d("not 201", " not Uploaded");

            }


        }




    }//async*/

}

