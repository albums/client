package com.example.steve.bums;

/**
 * Created by Beth on 2/16/2015.
 */

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Beth on 2/16/2015.
 */
public class TabsFragment extends Fragment {
    public static final String TAG = "TabsFragment";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tabs, parent, false);


        Button homeButton = ((Button) v.findViewById(R.id.home_btn));
        homeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //((HomeActivity)getActivity()).changeFragment("NEWS");
                Intent open = new Intent(TabsFragment.this.getActivity(),ViewActivity.class);
                TabsFragment.this.getActivity().startActivity(open);
                TabsFragment.this.getActivity().finish();

            }
        });

        Button profileButton = ((Button) v.findViewById(R.id.prof_btn));
        profileButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //((HomeActivity)getActivity()).changeFragment("PROFILE");
                Intent open = new Intent(TabsFragment.this.getActivity(),ProfileActivity.class);
                TabsFragment.this.getActivity().startActivity(open);
                TabsFragment.this.getActivity().finish();
            }
        });


        return v;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {

        super.onPause();
    }

}

