package com.example.steve.bums;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class ViewAlbumActivity extends ActionBarActivity {

    private Fragment tabs;
    private ImageButton photoButton;
    String id_str ="";
    ProgressBar progressBar8;
    String result = "";
    private List<String> pics;
    private List<String> albumPic;
    private List<Bitmap> bitPic;
    Bitmap temp;
    Bitmap scaledBitmap;
    public final static String EXTRA_MESSAGE = "HOME.MESSAGE";
    TextView albumName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_album);
        //albumId = new ArrayList<String>();
        progressBar8 = (ProgressBar) findViewById(R.id.progressBar8);
        progressBar8.setVisibility(View.INVISIBLE);
        albumName = (TextView) findViewById(R.id.album_name);
        albumPic = new ArrayList<String>();
        bitPic = new ArrayList<Bitmap>();
        //Global Variables for user ID
        GlobalVars state = ((GlobalVars) getApplicationContext());
        id_str = state.getSomeVariable();
        Log.d("id_viewAlbum", id_str);
        albumName.setText(getAlbumName());
        //Create a set of dummy strings to be displayed
       // pics = new ArrayList<String>();

       // pics.add("asd");






        new PopluateAlbum().execute();

        //hide Action Bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Button back = (Button) findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent open = new Intent(ViewAlbumActivity.this, ProfileActivity.class);

                //use album name to interact with data base
                //EditText editText = (EditText) findViewById(R.id.edit_message);
                //String message = editText.getText().toString();
                open.putExtra(EXTRA_MESSAGE, "PROFILE");
                startActivity(open);
                //manager = getFragmentManager();
                finish();
                //if(addFriendsFragment == null) {
                  //  addFriendsFragment = new AddFriendsFragment();
                    //manager.beginTransaction().add(R.id.addFriendsFragment, addFriendsFragment)
                      //      .commit();
                }

            });
    }

    class PopluateAlbum extends AsyncTask<Void, Void, Integer> {


        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar8.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }
        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;

            int status = -1;

            try{

                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/albums/" + getAlbumId() +"/pictures"));

                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();

                // convert inputstream to string
                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            }catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            Log.d("result", result);
            Log.d("status",Integer.toString(status));


            try {
                JSONArray array = new JSONArray(result);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject currObject = array.getJSONObject(i);
                    albumPic.add (currObject.getString("encoded_pic"));
                    //Log.d("name", albumPic.get(i));
                    try {
                        /*
                        URL urlConnection = new URL(albumPic.get(i));
                        HttpURLConnection connection = (HttpURLConnection) urlConnection
                                .openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        bitPic.add(BitmapFactory.decodeStream(input));
                        input.reset();
                        */
                        byte[] decodedString = Base64.decode(albumPic.get(i), Base64.DEFAULT);
                        temp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        scaledBitmap = Bitmap.createScaledBitmap(temp, 360, 360,true);
                        bitPic.add(scaledBitmap);
                       // in.reset();
                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



            return status;

        }
        protected void onPostExecute (Integer status){




            progressBar8.setVisibility(View.INVISIBLE);

            // Create the adapter passing a reference to the XML layout for each row
            // and a reference to the EditText (or TextView) in the item XML layout
            ArrayAdapter adapter = new ArrayAdapter(ViewAlbumActivity.this,
                    R.layout.home_grid_items, bitPic) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View row = convertView;
                    final int pos = position;
                    if(row == null){
                        //getting custom layout for the row
                        LayoutInflater inflater = getLayoutInflater();//LayoutInflater.from(getActivity());
                        row = inflater.inflate(R.layout.home_grid_items, parent, false);
                    }

                    //get the reference to the EditText of your row.
                    //find the item with row.findViewById()
                    TextView picNameField = (TextView)row.findViewById(R.id.pic_name);
                    ImageView picBox = (ImageView)row.findViewById(R.id.album_pic);

                    picNameField.setText(Integer.toString(position + 1));


                    picBox.setImageBitmap(bitPic.get(position));




                    return row; //the row that ListView draws
                }



            };

            // Get a reference to our ListView
            GridView gridView = (GridView) findViewById(R.id.gridView2);



            // Set the adapter on the List View
            gridView.setAdapter(adapter);

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // Toast.makeText(getApplicationContext(),
                    //"Click ListItem Number " + position, Toast.LENGTH_LONG)
                    //.show();
                    Intent intent = new Intent(ViewAlbumActivity.this, PicturesActivity.class);
                    //Convert to byte array
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitPic.get(position).compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    intent.putExtra("bitmap", byteArray);
                    //use album name to interact with data base
                    //EditText editText = (EditText) findViewById(R.id.edit_message);
                    //String message = editText.getText().toString();
                    //intent.putExtra(EXTRA_MESSAGE, message);
                    startActivity(intent);
                    //finish();
                }
            });

            photoButton = ((ImageButton) findViewById(R.id.photo_button));
            photoButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //InputMethodManager imm = (InputMethodManager) getActivity()
                    //        .getSystemService(Context.INPUT_METHOD_SERVICE);
                    //imm.hideSoftInputFromWindow(mealName.getWindowToken(), 0);
                    startCamera();
                }
            });

        }


    }

    public String getAlbumId(){
        Bundle extras;
        extras = getIntent().getExtras();
        return extras.getString("album_Id");
    }

    public String getAlbumName(){
        Bundle extras;
        extras = getIntent().getExtras();
        return extras.getString("album_name");
    }

    public void startCamera() {
        //Fragment cameraFragment = new CameraFragment();
        //FragmentTransaction transaction = this.getFragmentManager()
        //        .beginTransaction();
        //transaction.replace(R.id.fragmentContainer3, cameraFragment);
        //transaction.addToBackStack("ViewAlbumFragment");
        //transaction.commit();
        Intent open = new Intent(this, CameraActivity.class);
        open.putExtra("album_Id", getAlbumId());
        open.putExtra("album_name", getAlbumName());

        startActivity(open);
        finish();

    }

    public String getUserId(){

        return id_str;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result2 = "";
        while((line = bufferedReader.readLine()) != null)
            result2 += line;

        inputStream.close();
        return result2;

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_album, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
