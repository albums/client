package com.example.steve.bums;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Created by Beth on 2/16/2015.
 */
public class AddFriendsFragment extends Fragment {
    public static final String TAG = "AddFriendsFragment";
    EditText addFriend;
    ProgressBar progressBar4;
    String result;
    boolean userFound = false;
    boolean flag = false;
    String userName;
    View theView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_friends, parent, false);
        theView = v;
        addFriend = (EditText)v.findViewById(R.id.field_friend_name);

        progressBar4 = (ProgressBar)v.findViewById(R.id.progressBar11);
        progressBar4.setVisibility(View.INVISIBLE);
        Button button = (Button) v.findViewById(R.id.btn_add);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(addFriend.getText().toString().length() != 0) {



                    new searchFriend().execute();
                    //set to global
                    //((AddNewAlbumActivity) getActivity()).setConUser(addFriend.getText().toString());

                   //todo Return from fragment to activity

                }
                else{

                    Toast.makeText(AddFriendsFragment.this.getActivity().getApplicationContext(), "Blank Field",
                    Toast.LENGTH_LONG).show();
                }


            }
        });

        return v;

    }


    class searchFriend extends AsyncTask<Void, Void, Integer> {


        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar4.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;

            int status = -1;

            try {

                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/users"));

                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();

                // convert inputstream to string
                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

                //JSONObject json2 = new JSONObject(result);

                if(((AddNewAlbumActivity) getActivity()).getUserName().equals(addFriend.getText().toString())){

                    flag = true;
                    Log.d("flag", Boolean.toString(flag));
                }


                if(flag == false) {
                    JSONArray array = new JSONArray(result);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject currObject = array.getJSONObject(i);
                        userName = currObject.getString("username");
                        Log.d("Username", userName);
                        Log.d("addFriend", addFriend.getText().toString());
                        Log.d("curr_username", ((AddNewAlbumActivity) getActivity()).getUserName());
                        if (userName.equals(addFriend.getText().toString())) {
                            userFound = true;
                            break;

                        }
                    }
                }

            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            Log.d("result", result);
            Log.d("status", Integer.toString(status));

            return status;

        }

        protected void onPostExecute (Integer status) {

            progressBar4.setVisibility(View.INVISIBLE);

            if (flag == true){

                Toast.makeText(AddFriendsFragment.this.getActivity().getApplicationContext(), "You can't add yourself",
                Toast.LENGTH_LONG).show();
            }
            else if(userFound == true){


                Toast.makeText(AddFriendsFragment.this.getActivity().getApplicationContext(), "Friend Added",
                Toast.LENGTH_LONG).show();

                ((AddNewAlbumActivity) getActivity()).setConUser(addFriend.getText().toString());
                TextView et = (TextView) ((AddNewAlbumActivity) getActivity()).findViewById(R.id.field_friends);
                EditText name = (EditText)theView.findViewById(R.id.field_friend_name);
                if(et.getText().toString().equals("")){
                    et.setText(name.getText().toString());
                }else {
                    et.setText(et.getText().toString() + ", " + name.getText().toString());
                }
                userFound = false;

            }
            else{
                Toast.makeText(AddFriendsFragment.this.getActivity().getApplicationContext(), "Username not found",
                Toast.LENGTH_LONG).show();
            }

            ((AddNewAlbumActivity)getActivity()).destroyFragment();
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {

        super.onPause();
    }

}