package com.example.steve.bums;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class ViewActivity extends ActionBarActivity {

    private Fragment tabsFragment;
    GlobalVars state;
    private FragmentManager manager;
    String id_str ="";

    String result = "";
    ProgressBar progressBar4;
    private List<String> albumOwnerID;
    private  String[] imageForList;
    View v;
    TextView friendCounts;
    private List<String> Albumname;
    private List<String> albumPic;
    private List<String> userNameNews;
    private List<Bitmap> bitPic;
    String listAlbum;
    Random rn = new Random();
    Bitmap temp;
    Bitmap scaledBitmap;
    int posAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        //Global Variables for user ID
        state = ((GlobalVars) getApplicationContext());
        id_str = state.getSomeVariable();
        //Log.d("id_home", id_str);

        manager = getFragmentManager();
        tabsFragment = manager.findFragmentById(R.id.fragmentContainer2);

        if(tabsFragment == null) {
            tabsFragment = new TabsFragment();
            manager.beginTransaction().add(R.id.fragmentContainer2, tabsFragment)
                    .commit();
        }

        //hide Action Bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        Albumname = new ArrayList<String>();
        progressBar4 = (ProgressBar) findViewById(R.id.progressBar5);
        progressBar4.setVisibility(View.INVISIBLE);

        albumPic = new ArrayList<String>();
        bitPic = new ArrayList<Bitmap>();
        albumOwnerID = new ArrayList<String>();
        userNameNews = new ArrayList<String>();
        //Create a set of dummy strings to be displayeddisplayed
        new PopluateList().execute();


    }
    public void destroyFragment(){

        if(tabsFragment != null)
            manager.beginTransaction().remove(tabsFragment).commit();
        tabsFragment = null;
    }
    public String getUserId(){

        return id_str;
    }

    public String getUserName(){

        return state.getUserName();
    }


    class PopluateList extends AsyncTask<Void, Void, Integer> {


        protected void onPreExecute() {
            //textView.setText("Hello !!!");

            progressBar4.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }
        @Override
        protected Integer doInBackground(Void... params) {
            InputStream inputStream = null;

            int status = -1;

            try{

                // create HttpClient
                HttpClient httpclient = new DefaultHttpClient();

                // make GET request to the given URL
                HttpResponse httpResponse = httpclient.execute(new HttpGet("http://ec2-54-148-129-99.us-west-2.compute.amazonaws.com:3000/pictures"));

                // receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();
                //receive response as code
                status = httpResponse.getStatusLine().getStatusCode();

                // convert inputstream to string
                if(inputStream != null)
                    result = convertInputStreamToString(inputStream);
                else
                    result = "Did not work!";

            }catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            Log.d("result", result);
            Log.d("status",Integer.toString(status));

            try {
                JSONArray array = new JSONArray(result);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject currObject = array.getJSONObject(i);
                    albumPic.add (currObject.getString("encoded_pic"));
                    Albumname.add (currObject.getString("name"));
                    userNameNews.add (currObject.getString("username"));
                    albumOwnerID.add (currObject.getString("owner_id"));
                    Log.d("pic ID", albumPic.get(i));
                    Log.d("Album name", Albumname.get(i));
                    Log.d("username", userNameNews.get(i));
                    Log.d("owner_id", albumOwnerID.get(i));
                    try {

                        byte[] decodedString = Base64.decode(albumPic.get(i), Base64.DEFAULT);
                        //InputStream in = new java.net.URL(albumPic.get(i)).openStream();
                        temp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        scaledBitmap = Bitmap.createScaledBitmap(temp, 360, 360,true);
                        bitPic.add(scaledBitmap);
                        // in.reset();
                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return status;

        }
        protected void onPostExecute (Integer status){

            progressBar4.setVisibility(View.INVISIBLE);
            final int temp = albumPic.size() - 1;

            ArrayAdapter adapter = new ArrayAdapter(ViewActivity.this,
                    R.layout.home_list_items, albumPic) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View row = convertView;
                    final int pos = position;
                    if(row == null){
                        //getting custom layout for the row
                        LayoutInflater inflater = ViewActivity.this.getLayoutInflater();//LayoutInflater.from(getActivity());
                        row = inflater.inflate(R.layout.home_list_items, parent, false);
                    }

                    TextView a_name = (TextView) row.findViewById(R.id.A_name);
                    TextView userNameField = (TextView)row.findViewById(R.id.user_name);

                    ImageView albumImage = (ImageView)row.findViewById(R.id.album_cover);

                    //userNameField.setText(users.get(position));
                    //TODO  set image in list view

                    if((temp - position) >= 0) {
                        a_name.setText(Albumname.get(temp - position));
                        userNameField.setText(userNameNews.get(temp - position));
                        albumImage.setImageBitmap(bitPic.get(temp - position));
                    }

                    Button star = (Button)row.findViewById(R.id.btn_star);
                    star.setText(Integer.toString(rn.nextInt(10 - 0 + 1) + 0));
                    //Log.e("num",Integer.toString(rn.nextInt(10 - 0 + 1) + 0)  );

                    userNameField.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            state.setfriendId(albumOwnerID.get(temp - pos));
                            state.setfriendName(userNameNews.get(temp - pos));

                            Log.d("Friend ID", albumOwnerID.get(temp - pos));
                            Log.d("Friend ID", userNameNews.get(temp - pos));

                            Intent intent = new Intent(view.getContext(), GotoFriendProfile.class);
                            startActivity(intent);
                        /*Intent intent = new Intent(context, MainActivity2.class);
                        context.startActivity(intent);*/

                        }
                    });
                    albumImage.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            state.setfriendId(albumOwnerID.get(temp - pos));
                            state.setfriendName(userNameNews.get(temp - pos));

                            Log.d("Friend ID", albumOwnerID.get(temp - pos));
                            Log.d("Friend ID", userNameNews.get(temp - pos));

                            Intent intent = new Intent(view.getContext(), GotoFriendProfile.class);
                            startActivity(intent);
                        /*Intent intent = new Intent(context, MainActivity2.class);
                        context.startActivity(intent);*/

                        }
                    });

                    return row; //the row that ListView draws
                }


            };

            // Get a reference to our ListView
            ListView listView1 = (ListView) findViewById(R.id.listView);

            // Set the adapter on the List View
            listView1.setAdapter(adapter);


        }


    }

        private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result2 = "";
        while((line = bufferedReader.readLine()) != null)
            result2 += line;

        inputStream.close();
        return result2;


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
